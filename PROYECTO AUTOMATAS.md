![](IMAGENES/Poster_Batalla_Naval..jpg)

# PROYECTO FINAL
**PROYECTO FINAL**

**Autómatas y Lenguajes Formales**

**Profesor:** Luis Carlos Guayacan

**Estudiantes:**

Deivyd Dario Parra - 2162916

Juan David Medina Hernández - 2191955

Brayan Arley Rojas Mayorga - 2191942

**Objetivo:**
Desarrollar en el juego de batalla naval utilizando lo aprendido de Autómatas Finitos Deterministas en cual permita la jugabilidad usando movimientos propios del juego en una maquina virtual.

**Definición Formal y gráfico del autómata:**

_Sea A el autómata definido_: A = (Q , ∑ , 𝛾 , q0 , F )

_El conjunto de estados:_ Q = {q0 , q1 , q2 , ! }

_Su alfabeto:_ ∑ = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'}

_El estado final:_ F = {!}

_Sus transiciones:_

𝛾( q0 , m ) = q1

𝛾( q0 , n ) = q2

𝛾( q1 , n ) = !

𝛾( q2 , m ) = !

> **Diagrama del Autómata**

![](IMAGENES/Proyecto_1.1.JPG)


AUTOMATA (AFD)

```python
from automatalib.fa.dfa import DFA
d = DFA(
    states={'q0', '!', 'q1', 'q2'},
            
    input_symbols={'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y'},
                  
    transitions={
        'q0': {'m': 'q1', 'n': 'q2'},
        'q1': {'n': '!'},
        'q2': {'m': '!'},
        '!': {}
    },
    initial_state='q0',
    final_states={'!'}
)
```
```python

prueba=input(("              | a | b | c | d | e | \n"
              "              | f | g | h | i | j | \n"
              "              | k | l | m | n | o | \n"
              "              | p | q | r | s | t | \n"
              "              | u | v | w | x | y | \n"
              "          Coordenadas del mapa de Guerra \n"))
try:
    print("¡Has destruido el barco!\n"
          "¡¡Enhorabuena!" + d.validate_input(prueba))
except Exception as e:
        print("Intenta de nuevo, has fallado \n", e)
```
   
![](IMAGENES/Proyecto_1.2.JPG)

![](IMAGENES/Proyecto_1.3.JPG)

**VIDEO PRESENTACIÓN:**

https://www.youtube.com/watch?v=ndmK5Zc-g1E&t=2s&ab_channel=DeividParra
